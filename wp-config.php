<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ch47411_word' );

/** MySQL database username */
define( 'DB_USER', 'ch47411_word' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e{p9u@3im:llNS6{sno}SkzaO+Ya33)Xo4lzI,Zm_Q?6{}K%><p#c7B<mgVw##5t' );
define( 'SECURE_AUTH_KEY',  'BgI,a0QtG9y.>|G5_-sGA|iHyu6F@k;?2xt]hQi8k`I[Ke6aWh$&clT073!f;a!z' );
define( 'LOGGED_IN_KEY',    'e@[D!bx9+0Uj-7C7p[v=pn_,pvj,(Y`PPS3C&R5na<J(-r5S,^v9+x}|aqqPMbU[' );
define( 'NONCE_KEY',        '/Xw*/:N>rptuaDpOWo(/BOLA||c/a2MbQV-q$J9I~[AZu_3sL+XOD{`h0$G%$j2F' );
define( 'AUTH_SALT',        'ANi_C=9TQa-{1RY;ja~%tl&XHR]^v,8Ri;2/MR&>1&sj;k.(f-LhRl[X,tz[nQF^' );
define( 'SECURE_AUTH_SALT', '/QC/$p]a1Hy#1K?,O+!hg+;tq6@!7mpV,ZraV[O/JkBNhi+oW/@h8`f=$&bJ-.nt' );
define( 'LOGGED_IN_SALT',   '[gWh+K+XZ-No[cDfdNLV!7:mA*>@*.-y:f+3is1[n<w8h!*H}qS|p}TO/n7.0*qt' );
define( 'NONCE_SALT',       'A1?pC>Z}mJY]Lm!E?+Ws7MbXh2mqZ&aU&F|DWP,1/gl(<l:LbOwg:LaQOK,-8r:8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
